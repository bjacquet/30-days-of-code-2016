module Submits where

import Text.Printf


{- #Day 0: Print Hello World!#

For this challenge, all you have to do is print the following two lines:

```
Hello World. 
Welcome to 30 Days of Code.
```
-}

day0 = putStr "Hello World.\nWelcome to 30 Days of Code."


{- #Day2: Arithmetic!#

If given the meal price, tip percentage, and tax percentage, we can find the
final price of a meal.

##Input Format##
Three numbers, (M, T, and X), each on seperate lines:
 - M will be a double representing the original price of the meal.
 - T will be a integer representing the precentage that the customer wants to
   tip based off of the original price of the meal.
 - X will be an integer representing the tax percentage that the customer has
   to pay based off of the original price of the meal.

##Output Format##
A string stating...

```
The final price of the meal is $-.
```

where the final price of the meal is substituted for the dash. The price should
be rounded to the nearest integer (dollar).
-}

finalPriceRounded p = round p

finalPrice :: Double -> Int -> Int -> Int
finalPrice m t x =
  let tip   = (m * (fromIntegral t)) / 100
      tax   = (m * (fromIntegral x)) / 100
  in finalPriceRounded (m + tip + tax)

f2 m t x = printf "The final price of the meal is $%d\n" (finalPrice m t x)

day2 :: IO()
day2 = do
  m <- getLine
  t <- getLine
  x <- getLine
  f2 (read m) (read t) (read x)


{- #Day3: If-Else Statements!#

Given an integer N as input, check the following:
  - If N is odd, print "Weird".
  - If N is even and, in between the range of 2 and 5(inclusive), print "Not
    Weird".
  - If N is even and, in between the range of 6 and 20(inclusive), print
    "Weird".
  - If N is even and N>20, print "Not Weird".
-}
weirdNotWeird :: Int -> IO()
weirdNotWeird n
  | odd n     = putStrLn "Weird"
  | n <= 5    = putStrLn "Not Weird"
  | n <= 20   = putStrLn "Weird"
  | otherwise = putStrLn "Not Weird"

day3 :: IO()
day3 = do
  n <- getLine
  weirdNotWeird (read n)


{- #Day 5: Loops!#

Given three integers a, b, and N, output the following series:

a+20b,a+20b+21b,......,a+20b+21b+...+2N−1b

##Input Format##
The first line will contain the number of testcases T. Each of the next T lines
will have three integers, a, b, and N.

##Constraints##
0≤T≤500
0≤a,b≤50
1≤N≤15
-}
calculateSerie :: Int -> Int -> Int -> Int -> Int -> IO()
calculateSerie a b n c s
  | c == n    = printf "\n"
  | c == 0    = let v =  a + b + s
                in do printf "%d " v
                      calculateSerie a b n (c +1) v
  | otherwise = let v = 2^c * b + s
                in do printf "%d " v
                      calculateSerie a b n (c +1) v

calculateSeries :: Int->IO()
calculateSeries 0 = return ()
calculateSeries n = do
   line <- getLine
   let values = words line
       a = read (head values)
       b = read (head (tail values))
       n = read (last values)
     in calculateSerie a b n 0 0
   calculateSeries (n -1)

day5 :: IO()
day5 = do
  n <- getLine
  calculateSeries (read n)


{- #Day 6: Let's Review!#

Print a staircase of height N that consists of # symbols and spaces as given in
Sample Output.

##Sample Input##

6

##Sample Output##

      #
     ##
    ###
   ####
  #####
 ######
-}
getSteps char 1 = char
getSteps char n = char ++ getSteps char (n-1)

getMeALadder :: Int->Int->IO()
getMeALadder s n
  | s == n    = putStrLn (getSteps "#" n)
  | otherwise = do putStrLn ((getSteps " " (s -n)) ++ (getSteps "#" n))
                   getMeALadder s (n +1)

day6 :: IO()
day6 = do
  n_temp <- getLine
  let n = read n_temp :: Int
  getMeALadder n 1

{- #Day 7: Arrays!#
##Input Format##

The first line of input contains N, the number of integers. The next line
contains N integers separated by a space.

Output Format

Print the N integers of the array in the reverse order on a single line
separated by a space.
-}
reverseMe :: [Int] -> [Int]
reverseMe [] = []
reverseMe (x:xs) = (reverseMe xs) ++ [x]

stringMe [] = ""
stringMe (x:xs) = show x ++ " " ++ stringMe xs

day7 :: IO ()
day7 = do
    n_temp <- getLine
    let n = read n_temp :: Int
    arr_temp <- getLine
    let arr = map read $ words arr_temp :: [Int]
        rra = reverseMe arr
    putStrLn (stringMe rra)


{- #Day 9: Recursion!#

Given two integers, x and y, their GCD (greatest common divisor) can be
calculated recursively using Euclid's Algorithm, which essentially says that
if x equals y, then GCD(x,y)=x; otherwise, GCD(x,y)=GCD(x−y,y) if x>y.
-}
gcd' x y
  | x == y    = x
  | otherwise = gcd' (x-y) y

day9 :: Integral a => a -> a -> a
day9 n m = let x = max n m
               y = min n m
           in gcd' x y


{- #Day 20: Review + More String Methods!#

Given a string S, find number of words in that string. For this problem a word
is defined by a string of one or more English letters.

Note: Space or any of the special characters like ![,?.\_'@+] will act as a
delimiter. 
-}
insertSpaces string = let repl '!' = ' '
                          repl '[' = ' '
                          repl ',' = ' '
                          repl '?' = ' '
                          repl '.' = ' '
                          repl '\\' = ' '
                          repl '_' = ' '
                          repl '\'' = ' '
                          repl '@' = ' '
                          repl '+' = ' '
                          repl ']' = ' '
                          repl c = c
                      in map repl string

day20 :: IO()
day20 = do
  string <- getLine
  let _words = words (insertSpaces string)
      howMany = length _words
  putStrLn (show howMany)
  mapM putStrLn _words
  return ()


{- #Day 26: Testing Part I + Implementations!#

The Head Librarian at a library wants you to make a program that calculates the
fine for returning the book after the return date. You are given the actual and
the expected return dates. Calculate the fine as follows:

  - If the book is returned on or before the expected return date, no fine will
be charged, in other words fine is 0.
  - If the book is returned in the same month as the expected return date, Fine
= 15 Hackos × Number of late days
  - If the book is not returned in the same month but in the same year as the
expected return date, Fine = 500 Hackos × Number of late months
  - If the book is not returned in the same year, the fine is fixed at 10000
Hackos.
-}
fine :: [Int] -> [Int] -> Int
fine due return =
  fineAux (head due) (head (tail due)) (last due) (head return) (head (tail return)) (last return)

fineAux :: Int -> Int -> Int -> Int -> Int -> Int -> Int
fineAux due_day due_month due_year return_day return_month return_year
  | return_year > due_year   = 10000
  | return_month > due_month = 500 * (return_month - due_month)
  | return_day > due_day     = 15 * (return_day - due_day)
  | otherwise                = 0

day26 :: IO()
day26 = do
  _return <- getLine
  _due <- getLine
  let due = map read $ words _due :: [Int]
      return = map read $ words _return :: [Int]
  putStrLn (show (fine due return))
