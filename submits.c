#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>


/* #Day 1: Data Types!#

For this challenge, consider the following inputs (you don't need to read any
input):
  - 5.35
  - 'a'
  - false
  - 100
  - "I am a code monkey"
  - true
  - 17.3
  - 'c'
  - "derp"

For each line above, print out if it is an instance of a primitive or
referenced type as well as which data type it is.
*/
void day1(void){
    printf("Primitive : Double\n");
    printf("Primitive : Character\n");
    printf("Primitive : Boolean\n");
    printf("Primitive : Integer\n");
    printf("Referenced : String\n");
    printf("Primitive : Boolean\n");
    printf("Primitive : Double\n");
    printf("Primitive : Character\n");
    printf("Referenced : String\n");
    return 0;
}


/* #Day 11: 2D-Arrays + More Review!#

Given a 6×6 2D Array, A:

1 1 1 0 0 0
0 1 0 0 0 0
1 1 1 0 0 0
0 0 0 0 0 0
0 0 0 0 0 0
0 0 0 0 0 0

We can find 16 hourglasses in A; we define an hourglass in A to be a subset of 
values with indexes falling in this pattern in A's graphical representation:

a b c
  d
e f g

The sum of an hourglass is the sum of the values within it.

Your task is to calculate the sum of every hourglass in some 2D Array, A, and 
print the largest value (maximum of the sums) as your answer.

##Input Format##
There are 6 lines of input, where each line contains 6 space-separated integers
 describing 2D Array A; every value in A will be in the inclusive range of −9 
to 9.

##Constraints##
−9≤A[i][j]≤9
0≤i,j≤5

##Output Format##
Print the largest (maximum) hourglass sum found in A.
*/

int calculate_hourglass (int arr[6][6], int i, int j){
    return arr[i][j] +
           arr[i][j+1] +
           arr[i][j+2] +
           arr[i+1][j+1] +
           arr[i+2][j] +
           arr[i+2][j+1] +
           arr[i+2][j+2];
}

int main(){
    int arr[6][6];
    for(int arr_i = 0; arr_i < 6; arr_i++){
       for(int arr_j = 0; arr_j < 6; arr_j++){
          scanf("%d",&arr[arr_i][arr_j]);
       }
    }
    int max_value = -63; // Lowest possible value.
    for(int i = 0 ; i < 4 ; i++){
        for(int j = 0 ; j < 4 ; j++){
            int temp_value = calculate_hourglass(arr, i, j);
            if (temp_value > max_value)
                max_value = temp_value;
        }
    }
    printf("%d\n", max_value);
    return 0;
}
