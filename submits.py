#!/usr/bin/env python3

""" #Day 4: Logical Operators + Class vs. Instance!#

You will create a class Person and write a constructor that takes an integer,
initial_Age. In this constructor, you should check that the initial_Age is not
negative because we can't have people with negative ages.

If the initial_Age is negative, set the instance's age equal to zero then print
"This person is not valid, setting age to 0." without the quotations..

Inside of this class, you will also create an instance variable called age and
if initial_Age is not negative, then you will set age to equal the value of
initial_Age. In addition, you will write an instance method, amIOld(), that
prints whether people are old or not to the console.

In amIOld(), do the following things:
  - If the age of the Person instance is less than 13, then print "You are
    young."
  - If the age of the Person instance is equal or greater than 13, but less 18,
    print "You are a teenager."
  - Otherwise, print "You are old."

In addition, create an instance function called yearPasses() that increases the
age of the person instance by one.
"""

class Person:
    def __init__(self,initial_Age):
       # Add some more code to run some checks on initial_Age
        if initial_Age < 0:
            print ("This person is not valid, setting age to 0.")
            self.age = 0
        else:
            self.age = initial_Age
    def amIOld(self):
        # Do some computations in here and print out the correct statement to
        # the console
        if self.age < 13:
            print ("You are young.")
        elif self.age < 18:
            print ("You are a teenager.")
        else:
            print ("You are old.")
    def yearPasses(self):
        # Increment the age of the person in here
        self.age = self.age + 1


""" #Day 12: Inheritance!#

Complete the Grade class by writing a class constructor (Grade(String, String,
int,int)) and a char calculate() method. The calculate method should return the
character representative of a Student's *Grade.
"""
class Student:
    def __init__(self,firstName,lastName,phone):
        self.firstName=firstName
        self.lastName=lastName
        self.phone=phone

    def display(self):
        print ("First Name:",self.firstName)
        print ("Last Name:",self.lastName)
        print ("Phone:",self.phone)


class Grade(Student):
    def __init__ (self, firstName, lastName, phone, score):
        Student.__init__(self, firstName, lastName, phone)
        self.score = score

    def calculate (self):
        if self.score < 40:
            return "D"
        elif self.score < 60:
            return "B"
        elif self.score < 75:
            return "A"
        elif self.score < 90:
            return "E"
        else:
            return "O"


""" #Day 13: Abstract Classes!#

Your task is to write just the MyBook class. The class MyBook mustn't be public.
The void display() method should print and label the respective title, author,
and price of the book's instance (with each value on its own line).
"""
from abc import ABCMeta, abstractmethod

class Book(object, metaclass=ABCMeta):
    def __init__(self,title,author):
        self.title=title
        self.author=author

    @abstractmethod
    def display(): pass

class MyBook():
    def __init__(self, title, author, price):
        Book.__init__(self, title, author)
        self.price = price

    def display(self):
        print("Title:", self.title)
        print("Author:", self.author)
        print("Price:", self.price)


""" #Day 14: All about Scope!#

In this challenge, you will create a program that takes N non-negative
integers as input and finds the greatest absolute difference between two of
the N integers, and then print this difference to the console.
"""
class Difference:
    def __init__(self, a):
        self.__elements = a
        self.maximumDifference = 0

    def computeDifference(self):
      for i in range(len(self.__elements)):
        for j in range(len(self.__elements[i:])):
          self.maximumDifference = max(self.maximumDifference,
                                       abs(self.__elements[i] - self.__elements[j]))


""" #Day 15: Linked List!#

You are also given a pointer head pointing to the head node of a linked list and
an integer data to add to the list. Create a new node with the given integer.
Insert this node at the tail of the linked list and return the head node. The
given head pointer may be null, meaning that the initial list is empty.

Code for input/output is already handled in the editor. You have to complete
the function insert given in the editor. It takes two arguments: the head node
of the linked list and an integer data to be inserted.
"""
class Node:
    def __init__(self):
        self.data = None
        self.next = None

class Solution:
    def display(self,head):
        current = head
        while current:
            print(current.data,end=' ')
            current = current.next

    def insert(self,head,data):
        new = Node()
        new.data = data
        if head == None:
            return new
        else:
            current = head
            while current.next != None:
                current = current.next
            current.next = new
            return head

def day15 ():
    mylist= Solution()
    T=int(input())
    head=None
    for i in range(T):
        data=int(input())
        head=mylist.insert(head,data)
    mylist.display(head)


""" #Day16: Sorting!#

The absolute difference between two integers, a and b, is |a−b|. The minimum
absolute difference between two integers in a list A of positive integers, is
the smallest absolute difference between any
two integers in A.

Given a list A={a0,a1,…,aN−1} of unsorted integers, find and print the pair (or
pairs) of elements having the minimum absolute difference.

Note: More than one pair of elements may have the same absolute difference.

##Input Format##

The first line contains a single integer N, denoting the length of list A.
The second line contains N space-separated integers, a0,a1,…,aN−1, representing
the elements in A.

Constraints

  - 2≤N≤2×105
  - −107≤Ai≤107
  - Ai≠Aj,where 0≤i<j≤N−1

##Output Format##

Print the space-separated pair of elements having the minimum absolute
difference in ascending order.
If more than one pair meets this criterion, print them consecutively, separated
by a space, inascending order on a single line. Because we are printing
space-separated pairs, some elements may appear more than once in your output.
"""
def day16 ():
    N = int(input())
    A = []
    for i in range(N):
        A.insert(0, int(input()))
    A.sort()
    print(A)
    min = A[1] - A[0]
    minimums = [A[1], A[0]]
    for i, v in enumerate(A[1:]):
        if v == A[-1]:
            minimums.reverse()
        else:
            newMin = A[i+2] - A[i+1]
            if newMin < min:
                min = newMin
                minimums = [A[i+2], A[i+1]]
            elif newMin == min:
                minimums.insert(0, A[i+1])
                minimums.insert(0, A[i+2])
    for i in minimums:
        print(i),


""" #Day 17! Exceptions!#

Create a class Calculator which consists of a single method power(int,int).
This method takes two integers, n and p, as parameters and finds np. If either
n or p is negative, then the method must throw an exception which says "n and
p should be non-negative".
"""
class Calculator:
    def power(self, n, p):
        try:
            assert n >= 0
            assert p >= 0
            return pow(n, p)
        except AssertionError:
            raise Exception ("n and p should be non-negative")

def day17():
    myCalculator=Calculator()
    T=int(input())
    for i in range(T):
        n,p = map(int, input().split())
        try:
            ans=myCalculator.power(n,p)
            print(ans)
        except Exception as e:
            print(e)


""" #Day 18: Queues & Stacks!#

Write the following four functions/methods in class Palindrome:

 - `void pushCharacter(char ch)`: Pushes a character onto a stack.
 - `void enqueueCharacter(char ch)`: Enqueues a character in a queue.
 - `char popCharacter()`: Pops and returns the top character.
 - `char dequeueCharacter()`: Dequeues and returns the first character.
"""
class Palindrome:
    def __init__ (self):
        self.queue = []
        self.stack = []

    def pushCharacter (self, ch):
        self.stack[:-1] = ch

    def enqueueCharacter (self, ch):
        self.queue[:-1] = ch

    def popCharacter (self):
        if self.stack != []:
            aux = self.stack[-1]
            self.stack = self.stack[:-1]
            return aux

    def dequeueCharacter (self):
        if self.queue != []:
            aux = self.queue[0]
            self.queue = self.queue[1:]
            return aux

def day18 ():
    # read the string s
    W=input()
    #Create the Palindrome class object
    p=Palindrome()

    l=len(W)
    # push all the characters of string s to stack
    for i in range(l):
        p.pushCharacter(W[i])
    #enqueue all the characters of string s to queue
    for i in range(l):
        p.enqueueCharacter(W[i])
    f=True
    '''
    pop the top character from stack
    dequeue the first character from queue
    compare both the characters
    '''
    for i in range(l):
        if p.popCharacter()!=p.dequeueCharacter():
            f=False
            break
    #finally print whether string s is palindrome or not.
    if f:
        print ("The word, "+W+", is a palindrome.")
    else:
        print ("The word, "+W+", is not a palindrome.")


# Common classes for Day 22 & 23

class Node:
    def __init__(self,data):
        self.right=self.left=None
        self.data = data

class Solution:
    def insert(self,root,data):
        if root==None:
            return Node(data)
        else:
            cur=Node(data)
            if data<=root.data:
                cur=self.insert(root.left,data)
                root.left=cur
            else:
                cur=self.insert(root.right,data)
                root.right=cur
        return root

    def getHeight (self, root):
        if None == root.left == root.right:
            return 1
        elif None == root.left:
            return 1 + self.getHeight(root.right)
        elif None == root.right:
            return 1 + self.getHeight(root.left)
        else:
            return 1 + max(self.getHeight(root.left), self.getHeight(root.right))

    def levelOrderAux (self, root, roots):
        print(root.data, end=' ')
        if root.left:
            roots.append(root.left)
        if root.right:
            roots.append(root.right)
        if [] == roots:
            return
        self.levelOrderAux(roots.pop(), roots)

    def levelOrder (self, root):
        self.levelOrderAux(root, [])


''' #Day 22: Binary Search Trees#

The height of a binary tree is the number of nodes on the largest path from
root to any leaf. You are given a pointer root pointing to the root of a
binary search tree. Return the height of the tree.
You only have to complete the function getHeight given in the editor.

##Input Format##

First line contains T, the number of test cases. Next T lines contain an integer
data to be added to the binary search tree.

##Output Format##

Output the height of the binary search tree.
'''
def day22 ():
    T=int(input())
    myTree=Solution()
    root=None
    for i in range(T):
        data=int(input())
        root=myTree.insert(root,data)
    height=myTree.getHeight(root)
    print(height)


''' #Day 23: Review+Binary Trees!#

You are given a pointer root pointing to the root of a binary tree. You need to
print the level order traversal of this tree. In level order traversal, we
visit the nodes level by level from left to right.

For example:

         3
       /   \
      5     2
     / \    /
    1   4  6

For the above tree, the level order traversal is 3 -> 5 -> 2 -> 1 -> 4 -> 6.
'''
def day23 ():
    T=int(input())
    myTree=Solution()
    root=None
    for i in range(T):
        data=int(input())
        root=myTree.insert(root,data)
    myTree.levelOrder(root)


''' #Day 24: More Review + More Linked Lists!#

Given a pointer to the head node of a linked list whose data elements are in
non-decreasing order, you must delete any duplicate nodes and print the
updated list.

Code handling I/O is provided in the editor. Complete the removeDuplicates(Node)
function.

Note: The head pointer may be null, indicating that the list is empty. Be sure
to reset your next pointer when performing deletions to avoid breaking the list.
'''
class Node:
    def __init__(self,data):
        self.data = data
        self.next = None
class Solution:
    def insert(self,head,data):
            p = Node(data)
            if head==None:
                head=p
            elif head.next==None:
                head.next=p
            else:
                start=head
                while(start.next!=None):
                    start=start.next
                start.next=p
            return head
    def display(self,head):
        current = head
        while current:
            print(current.data,end=' ')
            current = current.next

    def removeDuplicates (self, head):
        last = None
        iterator = head
        while iterator:
            if iterator.data != last:
                last = iterator.data
                print (last, end=' ')
            iterator = iterator.next

def day24 ():
    mylist= Solution()
    T=int(input())
    head=None
    for i in range(T):
        data=int(input())
        head=mylist.insert(head,data)
    head=mylist.removeDuplicates(head)
    mylist.display(head);
