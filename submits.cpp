#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <string>

using namespace std;


/* #Day 19: Interfaces!#

Given an interface `AdvancedArithmetic` which contains a method signature `int
divisorSum(int n)`. (The `divisorSum` function just takes an integer as input
and return the sum of all its divisors.) Your only task is to write a class
`Calculator` which implements the interface.
*/
class AdvancedArithmetic{
    public:
          virtual int divisorSum(int n)=0;
};

class Calculator : public AdvancedArithmetic {
public:
  Calculator(){}

  int divisorSum (int n) {
    int out = 0;
    for(int aux = n ; aux >= 0 ; aux--){
      if ( 0 == remainder(n, aux))
	out += aux;
    }
    return out;
  }
};


/* #Day 21: Generics!#

Let's say you have an integer array and a string array. You have to write a
single method printArray that can print all the elements of both arrays. The
method should be able to accept both integer arrays or string arrays.
*/
template <class T>
T printArray(std::vector<T> v) {
  for(int i = 0 ; i != v.size() ; i++){
    cout << v[i] << endl;
  }
  return 0;
}

int day21() {
    vector<int> vInt{1, 2, 3};
    vector<string> vString{"Hello", "World"};
    
    printArray<int>(vInt);
    printArray<string>(vString);
    
    return 0;
}
