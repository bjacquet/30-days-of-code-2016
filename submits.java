import java.util.Scanner;
import java.util.regex.*;


/* #Day 28: RegEx, Patterns, and Intro to Databases!#

**Note:** This is a java only challenge, a RegEx is only valid if you can
**compile** it using the Pattern.compile method. You may find using a try-catch
block helpful here.

##Input Format##

The first line of input contains an integer, T (the number of test cases).
The T subsequent lines of test cases each contain a string of characters
describing a RegEx.

##Constraints##
1≤T≤100

##Output Format##

On a new line for each test case, print Valid if the given RegEx's syntax is
correct; otherwise, print Invalid.
*/
public class Solution {
   public static void main(String[] args) {
      Scanner in = new Scanner(System.in);
      int testCases = Integer.parseInt(in.nextLine());
      while(testCases>0){
         String pattern = in.nextLine();
	 //Write your code
	 try {
	     Pattern.compile(pattern);
	     System.out.println("Valid");
	 } catch (PatternSyntaxException ex) {
	     System.out.println("Invalid");
	 }
	 testCases--;
      }
   }
}
