
;; #Day 8: Dictionaries and Maps!#

;; You are given a phone book that consists of your friend's names and their
;; phone number. After that you will be given your friend's name as query. For
;; each query, print the phone number of your friend.

;; ##Input Format##
;; The first line will have an integer N denoting the number of entries in the
;; phone book. Each entry consists of two lines: a name and the corresponding
;; phone number.
(defparameter *address.book* (make-hash-table :test 'equalp))

(defun add.contacts (n)
  (loop repeat n
        do (let ((name (read-line))
                 (number (read-line)))
                (setf (gethash name *address.book*) number))))

(defun show.contact (name)
  (multiple-value-bind (number present) (gethash name *address.book*)
    (if present
      (format t "~a=~a~%" name number)
      (format t "Not found~%"))))

(defun day8 ()
  (let ((num.contacts (read)))
    (add.contacts num.contacts)
    (loop (multiple-value-bind (name end) (read-line)
	    (show.contact name)
	    (when end (return t))))))


;; #Day 10: Binary Numbers!#

;; For this challenge, convert a given number, n, from decimal (base 10) to
;; binary (base 2).

;; ##Input Format##
;; The first line contains a single integer, T, the number of test cases. The T
;; subsequent lines of test cases each contain a single value, n, the base 10
;; positive integer to be converted.
(let ((binary.numbers (make-hash-table :test 'equalp)))

  (defun dec-bin (number &optional (i 0))
    (multiple-value-bind (q r) (floor number 2)
      (let ((b (if (= r 0) 0 (expt 10 i))))
	(if (zerop q)
	    b
	  (+ b (dec-bin q (1+ i)))))))

  (defun binary (n)
    (multiple-value-bind (binary present) (gethash n binary.numbers)
      (if present
	  binary
	(setf (gethash n binary.numbers) (dec-bin n))))))

(defun day10 ()
  (loop repeat (read)
	do (format t "~a~%" (binary (read)))))


;; #Day 25: Running Time and Complexity!#

;; Determine if a given number X is prime or not. A prime number is a natural
;; number greater than 1 that has no positive divisors other than 1 and itself.
;; You will be given N numbers and for each, you will print out "Prime" if the
;; number is prime or "Not prime" if the number is not prime.
(defun primep (number)
  (cond ((or (and (> number 2) (evenp number))
	     (< number 2))
	 "Not prime")
	((= number 2)
	 "Prime")
	(t (loop for i from 3 to (sqrt number) by 2
		 do (when (= 0 (mod number i))
		      (return "Not prime"))
		 finally (return "Prime")))))

(defun day25 ()
  (loop repeat (read)
	do (format t "~a~%" (primep (read)))))


;; #Day 29: Look at Everything We've Learned!#

;; Suppose you have some string S having length N that is indexed from 0 to N−1.
;; You also have some string R that is the reverse of string S. S is funny if
;; the condition |Si−Si−1|=|Ri−Ri−1| is true for every i from 1 to N−1.

;; Note: For some string str, stri denotes the ASCII value of the ith 0-indexed
;; character in str. The absolute value of some integer, x, is written as |x|.

;; ##Input Format##

;; The first line contains an integer, T (the number of test cases).
;; The T subsequent lines each contain one string S.

;; ##Constraints##
;; 1≤T≤10
;; 2≤length of S≤10000

;; ##Output Format##

;; For each string S, print whether it is Funny or Not Funny on a new line
;; (i.e.: the ith line of output should be the answer for input string Si).
(defun funnyp (string gnirts)
  (loop for i across string
	for j across (subseq string 1)
	for x across gnirts
	for y across (subseq gnirts 1)
	do (when (/= (abs (- (char-code j) (char-code i)))
		     (abs (- (char-code y) (char-code x))))
	     (return "Not funny"))
	finally (return "Funny")))

(defun day29 ()
  (loop repeat (read)
	for string = (read-line)
	do (format t "~a~%" (funnyp string (reverse string)))))
