My submits to the [*30 Days Of Code*](https://www.hackerrank.com/contests/30-days-of-code/challenges/)
challenges on [HackerRank](https://www.hackerrank.com/).


References:
  Day 0: Print Hello World!                        -> submits.hs
  Day 1: Data Types!                               -> submits.c
  Day 2: Arithmetic!                               -> submits.hs
  Day 3: If-Else Statements!                       -> submits.py
  Day 4: Logical Operators + Class vs. Instance!   -> submits.py
  Day 5: Loops!                                    -> submits.hs
  Day 6: Let's Review!                             -> submits.hs
  Day 7: Arrays!                                   -> submits.hs
  Day 8: Dictionaries and Maps!                    -> submits.lisp
  Day 9: Recursion!                                -> submits.hs
  Day 10: Binary Numbers!                          -> submits.lisp
  Day 11: 2D-Arrays + More Review!                 -> submits.c
  Day 12: Inheritance!                             -> submits.py
  Day 13: Abstract Classes!                        -> submits.py
  Day 14: All about Scope!                         -> submits.py
  Day 15: Linked List!                             -> submits.py
  Day 16: Sorting!                                 -> submits.py
  Day 17: Exceptions!                              -> submits.py
  Day 18: Queues & Stacks!                         -> submits.py
  Day 19: Interfaces!                              -> submits.cpp
  Day 20: Review + More String Methods!            -> submits.hs
  Day 21: Generics!                                -> submits.cpp
  Day 22: Binary Search Trees                      -> submits.py
  Day 23: Review+Binary Tress!                     -> submits.py
  Day 24: More Review + More Linked Lists!         -> submits.py
  Day 25: Running Time and Complexity!             -> submits.lisp
  Day 26: Testing Part I + Implementations!        -> submits.hs
  Day 27: Tesing Part II!                          -> submits.sh
  Day 28: RegEx, Patterns, and Intro to Databases! -> submits.java
  Day 29: Look at Everything We've Learned!        -> submits.lisp
